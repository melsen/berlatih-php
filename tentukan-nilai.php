<?php
function tentukan_nilai($number)
{
    //  kode disini
    $output = "";
    if($number >= 85 && $number < 101)
    {
    	$output .= "98 Sangat Baik" . "<br>";
    }
    else if($number >= 70 && $number < 86)
    {
    	$output .= "76 Baik" . "<br>";
    }
    else if($number >= 60 && $number < 70)
    {
    	$output .= "67 Cukup" . "<br>";
    }
    else
    {
    	$output .= "43 Kurang" . "<br>";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>