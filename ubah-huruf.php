<?php
function ubah_huruf($string){
//kode di sini
	$abjad = "abcdefghijklmnopqrstuvwxyz";
	$hasil = "";
	for ($i=0; $i < strlen($string); $i++) 
	{ 
		# code...
		$position = strrpos($abjad, $string[$i]);
		$hasil .= substr($abjad, $position + 1, 1);
	}
	return $hasil  . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>